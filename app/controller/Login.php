<?php

require_once('../app/core/Controller.php');

class Login extends Controller {
    public function index() {
        $data["title"] = "Login";
        $data["msg"] = "";
        $this->view('auth/login', $data);
    }

    public function storeLogin() {
        if($this->model('User_model')->findUserBy($_POST["username"], $_POST["username"])) {
            $loggedUser = $this->model('User_model')->login($_POST);

            if($loggedUser) {
                $this->model('User_model')->createSession($loggedUser);
            } else {
                $data["title"] = "Login";
                $data["msg"] = "Wrong password";
                $this->view('auth/login', $data);
            }
        } else {
            $data["title"] = "Login";
            $data["msg"] = "Cant find the username";
            $this->view('auth/login', $data);
        }
    }
}
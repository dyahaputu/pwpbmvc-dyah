<?php

require_once('../app/core/Controller.php');

class Register extends Controller {
    public function index() {
        $data['title'] = 'Register';
        $this->view('auth/register', $data);
    }

    public function store() {
        if($this->model("User_model")->createUser($_POST)) {
            //Flasher::setFlash("el", "register", "success");
            header('Location:'. BASE_URL .'login/index');
            exit;
        }
    }
}